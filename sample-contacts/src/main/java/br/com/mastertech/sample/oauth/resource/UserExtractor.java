package br.com.mastertech.sample.oauth.resource;

import br.com.mastertech.sample.oauth.models.User;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UserExtractor implements PrincipalExtractor {
    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Integer id = (Integer) map.get("id");
        String name = (String) map.get("name");
        return new User(id, name);
    }
}
