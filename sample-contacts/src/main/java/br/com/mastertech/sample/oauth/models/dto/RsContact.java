package br.com.mastertech.sample.oauth.models.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class RsContact {

    @JsonProperty("name")
    private final String name;

    @JsonProperty("number")
    private final String number;

    @JsonProperty("ownerId")
    private final long ownerId;

    @JsonProperty("id")
    private final long id;

    public RsContact(String name, String number, long ownerId, long id) {
        this.name = name;
        this.number = number;
        this.ownerId = ownerId;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public String getName() {
        return name;
    }
}
