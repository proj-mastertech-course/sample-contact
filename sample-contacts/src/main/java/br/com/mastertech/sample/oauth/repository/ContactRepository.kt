package br.com.mastertech.sample.oauth.repository

import br.com.mastertech.sample.oauth.models.Contact
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ContactRepository : JpaRepository<Contact, Long> {

    fun findContactsByIdUserOwner(idUserOwner: Long): List<Contact>
}