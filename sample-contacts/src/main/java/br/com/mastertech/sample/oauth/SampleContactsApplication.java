package br.com.mastertech.sample.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleContactsApplication {
	public static void main(String[] args) {
		SpringApplication.run(SampleContactsApplication.class, args);
	}
}
