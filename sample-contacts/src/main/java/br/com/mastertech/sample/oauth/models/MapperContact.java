package br.com.mastertech.sample.oauth.models;

import br.com.mastertech.sample.oauth.models.dto.RqContact;
import br.com.mastertech.sample.oauth.models.dto.RsContact;

import java.util.ArrayList;
import java.util.List;

public class MapperContact {

    public static Contact fromRequestModelToModel(long idUser, RqContact rqContact) {
        return new Contact(idUser, rqContact.getName(), rqContact.getNumber());
    }

    public static RsContact fromModelToResponseModel(Contact contact) {
        return new RsContact(contact.getName(), contact.getPhoneNumber(), contact.getIdUserOwner(), contact.getId());
    }

    public static List<RsContact> fromModelsToRsModels(List<Contact> contacts) {
        List<RsContact> rsContacts = new ArrayList<>();
        for (Contact c : contacts) {
            rsContacts.add(fromModelToResponseModel(c));
        }
        return rsContacts;
    }
}
