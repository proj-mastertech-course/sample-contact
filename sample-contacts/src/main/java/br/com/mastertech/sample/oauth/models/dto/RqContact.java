package br.com.mastertech.sample.oauth.models.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class RqContact {

    @JsonProperty("name")
    private final String name;
    @JsonProperty("number")
    private final String number;

    public RqContact(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }
}
