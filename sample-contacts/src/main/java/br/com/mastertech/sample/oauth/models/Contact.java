package br.com.mastertech.sample.oauth.models;

import javax.persistence.*;

@Entity
@Table(name="contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "id_user_owner")
    private long idUserOwner;

    @Column(name = "name")
    private String name;

    @Column(name = "phone_number")
    private String phoneNumber;

    public Contact() {}

    public Contact(long idUserOwner, String name, String phoneNumber) {
        this.idUserOwner = idUserOwner;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public long getId() {
        return id;
    }

    public long getIdUserOwner() {
        return idUserOwner;
    }
}
