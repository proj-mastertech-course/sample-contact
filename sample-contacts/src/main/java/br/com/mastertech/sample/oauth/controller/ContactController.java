package br.com.mastertech.sample.oauth.controller;

import br.com.mastertech.sample.oauth.models.Contact;
import br.com.mastertech.sample.oauth.models.MapperContact;
import br.com.mastertech.sample.oauth.models.User;
import br.com.mastertech.sample.oauth.models.dto.RqContact;
import br.com.mastertech.sample.oauth.models.dto.RsContact;
import br.com.mastertech.sample.oauth.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContactController {

    @Autowired
    private ContactService service;


    @PostMapping("/save/contact/")
    public RsContact save(@AuthenticationPrincipal User user, @RequestBody RqContact rqContact) {
        if (user == null) {
            return null;
        }
        Contact contact = MapperContact.fromRequestModelToModel(user.getId(), rqContact);
        contact = service.save(contact);
        return MapperContact.fromModelToResponseModel(contact);
    }

    @GetMapping("/contacts")
    public List<RsContact> get(@AuthenticationPrincipal User user) {
        if (user == null) {
            return null;
        }
        List<Contact> contacts = service.getContactsByOwnerId(user.getId());
        return MapperContact.fromModelsToRsModels(contacts);
    }

}
