package br.com.mastertech.sample.oauth.service;

import br.com.mastertech.sample.oauth.models.Contact;

import br.com.mastertech.sample.oauth.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService {

    @Autowired
    private ContactRepository repository;


    public Contact save(Contact contact) {
        return repository.save(contact);
    }

    public List<Contact> getContactsByOwnerId(long ownerId) {
        return repository.findContactsByIdUserOwner(ownerId);
    }



}
