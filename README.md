Crie um microsserviço que guarde contatos telefônicos dos usuários.
Quando o usuário chamar POST /contato, ele deve criar um contato para o usuário logado.
Quando o usuário chamar o GET /contatos, ele deve listar todos os contatos deste usuário.

Obs: O objetivo principal aqui, é gerir os contatos por usuário, de maneira transparente. 